A Few Python Tips
=================

Context
-------

[PyLadies Southwest Florida talk](https://www.meetup.com/PyLadies-SWFL/events/274417833/) 19 Nov 2020

> Sumana will share a few tips that make it easier to work effectively with Python -- “a little taste of a lot of useful stuff”. She will discuss the initial setup, tools, best practices, and a few favorite modules.
> In the second part of her presentation, Sumana will talk about her "odd" journey to software project management with the hope of providing some solace and encouragement to those who consider transitioning to a career in tech.



Intro
-----

Thank you for coming.

This is "A Few Python Tips". I am presenting a few tips to help you work effectively with Python. This talk is aimed at experienced programmers who are accustomed to other scripting languages, and to new programmers for whom Python is their first language. It's based on a talk I gave back in 2014 and the notes from that are up at https://web.archive.org/web/20141118092852/http://opensourcebridge.org/wiki/2014/A_Few_Python_Tips .

This talk will *not* discuss: programming basics, why you might choose to work in Python, web frameworks such as Django and Flask, very specific Python packaging projects such as wheel and setuptools, or scientific computing distributions such as scipy and numpy.

I am aiming for breadth rather than depth, because the point here is to give you a little taste of a lot of useful stuff, and show you just enough to see why I use what I use. So I'm not going to go into a lot of detail for any one thing.

Initial setup tips
------------------

2 or 3? So, you have a choice of writing code in Python 2 or Python 3. Python 3 came out in 2008 and lots of people and organizations have been slowly transitioning to Python 3. If you are writing new code, I advise you to write it in Python 3. There are nearly no libraries and dependencies out there that can only work with Python 2, the old version, and that's less and less of a problem over time.

More info about that: https://www.python.org/doc/sunset-python-2/

The examples I'm going to give you are Python 3.

Installing Python onto your computer is a complicated enough issue that I'm not going to talk about it in any depth today. It depends a lot on what operating system you are running, and whether everyone in your organization is doing things in some particular way, and whether you are doing a lot of science and data analysis.

There's one more aspect of setup I'm going to talk about, and that is the world of packages, installing packages, and virtual environments.

https://packaging.python.org

[explain the idea of writing reusable little bits; some of them come bundled with Python, the "standard library", and some don't]

[need some way to pass them around - we call them "packages"]

[explain PyPI]


The Python Package Index, also known as PyPI. That's where most of the Python ecology lives, in terms of reusable Python code.

pypi.org -- run by the Python Software Foundation, which is a nonprofit. During the Q&A feel free to ask me why that is important!


Python 3, by default, comes with pip. pip is the official, supported package installer for Python. Users use pip to download and install packages from the Python Package Index and other places.

We'd like to know more about how to improve pip's documentation - one of our user experience experts said

"I'm looking for pip users to talk to about documentation, especially to discuss:

- what project documentation do you love?
- how we can improve pip's docs?"

https://bit.ly/pip-docs-interview

Big changes coming to pip in 20.3: https://pip.pypa.io/en/latest/user_guide/#changes-to-the-pip-dependency-resolver-in-20-3-2020

It might take you half an hour or two hours to go through https://packaging.python.org/tutorials/distributing-packages/ and learn how to make a package, and I recommend it, because then it'll demystify the process for you. And then if you have a chunk of code you want your colleagues or friends to be able to use, you can make a package and they can pip install it.


I can show you what I have installed via

``pip freeze``

Let me show you me installing something via pip.

``pip install simplejson``

So you can see that I have a lot of stuff here. And it's great that I have access to all those modules, but this causes two problems. One is, what if I start working on two projects that require different versions of a particular module? Like, one of your codebases needs pytwitter version 1, and the other needs pytwitter version 2. And second, how do I keep track of, and easily tell someone else, what dependencies a project requires?

So we use virtual environments. Sometimes you'll hear people call them virtualenvs or venvs.

For installation instructions, go to https://packaging.python.org/tutorials/installing-packages/#creating-virtual-environments

Within a virtual environment, you can just pip install something, with no sudo, and then it's only in that environment.

More on how virtualenvs work, with bash: https://www.recurse.com/blog/14-there-is-no-magic-virtualenv-edition

I use "virtualenvwrapper" which provides a little syntactic sugar and tracks where all the virtual environments are. See http://virtualenvwrapper.readthedocs.org/en/latest/install.html to install that. So with virtualenv and virtualenvwrapper installed, I can:

``mkvirtualenv testfoo``

``pip freeze``

``pip install simplejson``

``pip freeze``

``pip uninstall simplejson``

``deactivate``

``workon [tab]``

``rmvirtualenv testfoo``


Tools: Interpreters
-------------------

Python comes with a default tool that you might be interested in looking at further: the interpreter, the command-line environment where you can type one line of code at a time and see what Python does with it. You'll also hear it called a REPL, Read-Evaluate-Print Loop. When you install Python it comes with the stock interpreter.


``python3``

Now, in the interpreter:


``a = 2``

``a``

~~~
def foo():
    print("yay")
~~~

``foo()``

``help(a)``

And just like on the command line, you can easily go back to something you've done in the past:

``[up arrow]``

``blah Control-C``

``[up arrow]``

``Control-R   a =``

``quit()``


But I also want to mention anothe interpreter to you. It is great for exploring new stuff. It's called bpython. You install it using pip:

``pip install bpython``


And then, you can run it by calling it on the command line:

``bpython``


In the session:


``a = "wiki"``

``a.     [tab]      (``

It pops up information about what you can do.


``import s  [tab]``

So, if I'm trying out a new library:

``import mwclient``

``mwclient.``

``mwclient.Site( ``

And it lets you undo something you've just done.


``b = 2``

``b = "mag"``

``Control-R``

``b``

so I like it for exploring. 



Best practices: Debugging
-------------------------

A lot of people don't know about the -i option when you run a Python script. Basically, it runs the script, and then it dumps you into an interactive session with the Python interpreter, at the state it was in at the end of the script. So you can dig around and see what values are stored.

``less interaction-example.py``

``python -i interaction-example.py``

``dir()``

``toprint``

python -i spits you out after the script has finished running, or crashed. But maybe you want to see what happens just before it crashes. Or you want to be able to sketch, figure out what you want to happen next, just before it crashes. So another thing you can do is use pdb, the Python debugger. If you've ever set breakpoints in a program so that you can step through it, then this will be familiar to you.

``less demo-of-pdb.py``

``python demo-of-pdb.py``

See where I say ``set_trace`` ? That's what sets the breakpoint.

The pdb environment is different from the regular Python interpreter. It lets you step through lines with the "step" or "s" command, or continue to the next breakpoint with the "continue" or "c" command, or evaluate expressions.


``s``


``s``

``print a``

``c``

``c``

The most common thing you'll do is set traces. But there's a lot more you can do. There was a talk at PyCon http://pyvideo.org/video/2673/in-depth-pdb and there's a reference manual at https://docs.python.org/3/library/pdb.html . https://docs.python.org/3/library/pdb.html#debugger-commands

One more debugging tip is, when you're just starting out, you may want to know what you've actually defined, and whether or not something is in your path, so Python knows how to get to it.

``bpython``

``dir()``

``a = 1``

``dir()``

So you can see that if you have successfully defined a variable, it shows up in the list of strings in the current scope. I called it here so it's returning what's in the global scope.

``import sys``

``dir()``

``sys.path``

So if you've tried to install some Python library but it doesn't show up in sys.path, then that's something you can follow up on.


I love these two tools so much that I made a little zine about them: https://www.harihareswara.net/sumana/2016/10/15/0



A few favorite modules
----------------------



random!

``bpython``

``import random``

``a = range(1,15)``

``a``

random.choice just gives you a random choice from a list.

``random.choice(a)``

And since you're so often doing something like this, you don't even have to do that initial step of creating the range.

``random.randrange(15)``

random.sample gives you k unique items from a list.

``random.sample(a, 4)``

random.shuffle will rewrite the sequence in a list, in-place.

``namelist = ['Zack', 'Nandini', 'Anne', 'Nnedi', 'Isabel', 'Julia', 'John', 'Joseph', 'Adams', 'Lou', 'Anders', 'Richardson', 'Beth', 'Feruglio', 'Belle']``

``# Let's shuffle it.``


``random.shuffle(namelist)``

``namelist``

``# namelist is now ordered differently``


And if you just want a random float between 0 and 1, there's random.random().

``random.random()``


``ics`` is part of the standard library and lets you read from and write to calendar files!

https://gitlab.com/brainwane/MoMI-ics-cleanup/-/blob/master/create-fixed-ics.py

There's a great video of a talk by Christine Spang, [" "Email as Distributed Protocol Transport: How Meeting Invites Work and Ideas for the Future" talk at Open Source Bridge 2015"](https://www.youtube.com/watch?v=Jfg3CVrjsU0), which introduced me to hacking with the iCalendar format.

Arrow is a package that is NOT in the standard library and it's worth looking into. I was frustrated that `datetime` could not Just Do The Right Thing regarding turning very datestamp-looking strings into `datetime` objects? Aha, Arrow is my answer! http://arrow.readthedocs.io/en/latest/index.html

https://gitlab.com/brainwane/form990s/-/blob/master/investigate.py


My personal journey
-------------------

I'm going to talk in broad spontaneous strokes here, but I spoke about my career in various interviews such as [a Software Developers Journey podcast](https://devjourney.info/Guests/120-SumanaHarihareswara.html) ([highlights and quotes](https://www.timbourguignon.fr/devjourney120/))