This is a fairly quick-and-don't-depend-on-this repository meant to help people follow along with a presentation I'm giving on 2020-11-19.

Disclaimers:

* I'm sure a lot of this guidance and code is simplified and not as precise as it could be.
* [![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

